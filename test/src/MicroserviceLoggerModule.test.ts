/**
 * MicroserviceLoggerModule tests
 *
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

const Sandal = require('sandal');
import * as assert from "assert";
import {Config} from "mumba-config";
import {DiContainer} from "mumba-typedef-dicontainer";
import {MicroserviceLoggerModule} from '../../src/index';

describe('MicroserviceLoggerModule integration tests', () => {
	let container: DiContainer;

	beforeEach(() => {
		container = new Sandal();
		container.object('config', new Config({
			logger: {
				name: 'the-logger'
			}
		}))
	});

	it('should register "logger" in the container', (done) => {
		MicroserviceLoggerModule.register(container);

		assert.strictEqual(container.has('logger'), true);

		// TODO Add a TypeDef for a logger.
		container.resolve('logger', (err: Error, logger: any) => {
			if (err) {
				return done(err);
			}

			assert(logger.info, 'should be a Bunyan object');
			done();
		});
	});
});
