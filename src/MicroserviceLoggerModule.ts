/**
 * MicroserviceLoggerModule
 *
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

const bunyan = require('bunyan');

import {DiContainer} from "mumba-typedef-dicontainer";
import {Config} from "mumba-config";

/**
 * Registers "logger" - A Bunyan logger object.
 *
 * The Microservice can optionally add "defaultConfig" to the container but this must be done prior to registering the module.
 */
export class MicroserviceLoggerModule {
	/**
	 * Register the Config assets in the DI container.
	 *
	 * @param {DiContainer} container
	 */
	static register(container: DiContainer) {
		container
			.factory('logger', (config: Config) => {
				return bunyan.createLogger(config.get('logger'));
			});
	}
}
