# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]
- Fix up old typings format.

## [0.1.0] 18 Jul 2016
- Initial release.
