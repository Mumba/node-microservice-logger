# Mumba Logger Microservice Module

Provide logger support for a Microservice using Bunyan.

## DI Container

Adds a [Bunyan](https://www.npmjs.com/package/bunyan) object as "logger".

Requires that a `Config` object has been registered as `config`.

When the logger resolves, it expects that the Bunyan configuration is set in the `logger` path in the `Config` object.

## Installation

```sh
$ npm install --save mumba-microservice-logger
$ typings install -—save dicontainer=npm:mumba-typedef-dicontainer
```

## Examples

```typescript
const Sandal = require("sandal");
import {DiContainer} from "mumba-typedef-dicontainer";
import {Config} from "mumba-config";
import {MicroserviceLoggerModule} from "mumba-microservice-logger";

let container: DiContainer = new Sandal();

// Set the configuration for the logger.
// (or include the Microservice Config Module and set `defaultConfig`)
container.object('config', new Config({
	logger: {
		name: 'the-name-of-the-logger'
		// Add more configuration here.
	}
}));

// Register the module.
MicroserviceLoggerModule.register(container);

container.resolve('logger', (err: Error, logger: any) => {
	if (err) {
		throw err;
	}

	logger.info('It works!');
});
```

## Tests

To run the test suite, first install the dependencies, then run `npm test`:

```sh
$ npm install
$ npm test
```

## People

The original author of _Mumba Config Microservice Logger_ is [Andrew Eddie](https://gitlab.com/u/aeddie.mumba).

[List of all contributors](https://gitlab.com/Mumba/node-microservice-logger/graphs/master)

## License

[Apache 2.0](LICENSE.txt)

* * *

&copy; 2016 [Mumba Pty Ltd](http://www.mumba.cloud). All rights reserved.

